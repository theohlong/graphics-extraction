FROM pytorch/pytorch:1.4-cuda10.1-cudnn7-devel

COPY ./package-list.txt ./

RUN conda init bash
RUN conda config --append channels pytorch
RUN conda config --append channels nvidia
RUN conda config --append channels conda-forge

RUN conda create --name mathseer-pipeline --file package-list.txt
RUN conda activate mathseer-pipeline

COPY ./start.sh /start.sh
RUN chmod +x /start.sh

COPY ./gunicorn_conf.py /gunicorn_conf.py

COPY ./start-reload.sh /start-reload.sh
RUN chmod +x /start-reload.sh

COPY ./app /app
WORKDIR /app/

ENV PYTHONPATH=/app

EXPOSE 80

# Run the start script, it will check for an /app/prestart.sh script (e.g. for migrations)
# And then will start Gunicorn with Uvicorn
CMD ["/start.sh"]

