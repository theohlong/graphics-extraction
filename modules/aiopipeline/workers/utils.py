import os
from copy import copy
from io import BytesIO
from multiprocessing import Event
from typing import List
import time
import shutil
from pdf2image import convert_from_bytes, convert_from_path


def all_true(events: List[Event]) -> bool:
    for e in events:
        if not e.is_set():
            return False
    return True


async def check_status(resp, logger):
    if resp.status != 200:
        text = await resp.text()
        logger.info(f"got non 200 response from response with text:\n {text}")


def get_pdf_sizes(path_to_pdfs):
    pdf_sizes = {}
    pdfs = os.listdir(path_to_pdfs)
    for p in pdfs:
        pdf_sizes[os.path.basename(p)] = os.path.getsize(os.path.join(path_to_pdfs, p))/1000000
    return pdf_sizes


def print_filtered(path_to_pdfs, size_in_mb):
    pdfs = get_pdf_sizes(path_to_pdfs)
    for p in pdfs:
        if pdfs[p] > size_in_mb:
            print(pdfs[p], p)


def compare_img_gen_times(path_to_pdf):
    print(path_to_pdf)
    os.makedirs('tmp/fake', exist_ok=True)
    with open(path_to_pdf, 'rb') as f:
        pdf_bytes_io = BytesIO(f.read())
    start = time.time()
    convert_from_bytes(copy(pdf_bytes_io).read(), dpi=256)
    end = time.time()
    print("from bytes all in mem", str(end-start))
    start = time.time()
    convert_from_bytes(
                copy(pdf_bytes_io).read(),
                dpi=256,
                output_folder='tmp/fake')
    end = time.time()
    print("from bytes output folder", str(end-start))
    start = time.time()
    convert_from_bytes(
                copy(pdf_bytes_io).read(),
                dpi=256,
                output_folder='tmp/fake',
                paths_only=True)
    end = time.time()
    print("from bytes paths only", str(end-start))
    start = time.time()
    convert_from_path(path_to_pdf, dpi=256)
    end = time.time()
    print("from path all in mem", str(end-start))
    start = time.time()
    convert_from_path(
                path_to_pdf,
                dpi=256,
                output_folder='tmp/fake')
    end = time.time()
    print("from path output folder", str(end-start))
    start = time.time()
    convert_from_path(
                path_to_pdf,
                dpi=256,
                output_folder='tmp/fake',
                paths_only=True)
    end = time.time()
    print("from path paths only", str(end-start))
    shutil.rmtree('tmp/fake')



